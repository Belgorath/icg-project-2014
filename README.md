# Introduction to Computer Graphics project #

## How to switch between camera modes ##
main.cpp:48 - change the value ot cameraMode according to one of the values above

* MODE_AUTO
* MODE_FLY
* MODE_FPS


## How to hide/show the Bezier curve ##
main.pp:601 - change the boolean value of displayPath.


## How to enable/disable the 'growth' of the terrain ##
main.pp:602 - change the boolean value of progressiveHeight.


_EPFL - 2014_