#version 330 core
out vec4 color;

void main() {
	if (dot(gl_PointCoord-0.5,gl_PointCoord-0.5)>0.25) {
        discard;
    } else {
        color = vec4(1.0, 1.0, 0.5, 2-4*sqrt(pow(gl_PointCoord.s-0.5,2) + pow(gl_PointCoord.t-0.5,2)));
    }
    
}
