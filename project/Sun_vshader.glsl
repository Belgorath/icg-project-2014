#version 330 core 

uniform mat4 M;
uniform mat4 V;
uniform mat4 P;
uniform vec3 light_pos;

void main() {
    gl_Position = P * V * M * vec4(light_pos, 1.0);
    gl_PointSize = max(300 - 10*gl_Position.z, 100);
}
