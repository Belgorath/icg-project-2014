#pragma once
#include <Eigen/Dense>
#include <common.h>
#include <OpenGP/GL/Trackball.h>
#include "normal_table.h"

#define MOVE_STILL			1 // movement is stopped
#define MOVE_ACCELERATE		2 // movement is begining
#define MOVE_CRUISE			3 // movement has reached cruise speed
#define MOVE_DECELERATE		4 // movement is stopping

#define 	GLFW_KEY_A   65
#define 	GLFW_KEY_D   68
#define 	GLFW_KEY_E   69
#define 	GLFW_KEY_Q   81
#define 	GLFW_KEY_S   83
#define 	GLFW_KEY_W   87

namespace opengp{

/// @todo DOCUMENT
void cameraMove(void (*update_matrices)(const Eigen::Matrix4f& ));

namespace{
    static Eigen::Matrix4f scale_;
    static Eigen::Matrix4f rotation_;
    static Eigen::Matrix4f old_rotation_;
    static Eigen::Matrix4f translation_;
    static Trackball trackball_;
    static int width_;
    static int height_;
    static void (*_update_matrices)(const Eigen::Matrix4f&, vec3, vec3, vec3) = NULL;
    static vec3 c_look;
    static vec3 c_pos;
    static vec3 c_up;
    static vec3 c_direction;
    static float angle_displacement;
    static float angle_displacement_up;
    static vec3 cam_look_normal;
    static vec3 cam_up_normale;
    
    static bool movingForward = false;
    static bool movingBackward = false;
    static bool lookingLeft = false;
    static bool lookingRight = false;
    static bool lookingUp = false;
    static bool lookingDown = false;
	
	static int move_next = 0; // an index in the 'movement' table of normal_table.h
	static int move_state = MOVE_STILL;
        
    void update_matrices(){
        assert(_update_matrices!=NULL);
        Eigen::Matrix4f model = translation_ * rotation_ * scale_;
        _update_matrices(model, c_look, c_pos, c_up);
    }
    
    void resize(int width, int height){
        width_ = width;
        height_ = height;
        glViewport(0, 0, width, height);
    }
    
    vec3 cross(vec3 a, vec3 b) {
        return vec3(a[1]*b[2]-b[1]*a[2], a[2]*b[0]-b[2]*a[0], a[0]*b[1]-b[0]*a[1]);
    }
    
    void keyboard(int key, int action){
        c_direction = (c_look-c_pos).normalized();
        switch (key) {
            case GLFW_KEY_W:
				if (!movingBackward) {
				// otherwise we cannot start another movement
					if (action == GLFW_PRESS) {
						movingForward  = true;
						move_state = MOVE_ACCELERATE;
					} else {
						move_state = MOVE_DECELERATE;
					}
				}
				break;
            case GLFW_KEY_S:
				if (!movingForward) {
				// otherwise we cannot start another movement
					if (action == GLFW_PRESS) {
						movingBackward  = true;
						move_state = MOVE_ACCELERATE;
					} else {
						move_state = MOVE_DECELERATE;
					}
				}
				break;
            case GLFW_KEY_A: lookingLeft    = !lookingLeft; break;
            case GLFW_KEY_D: lookingRight   = !lookingRight; break;
            case GLFW_KEY_Q: lookingUp      = !lookingUp; break;
            case GLFW_KEY_E: lookingDown    = !lookingDown; break;
            case GLFW_KEY_ESC: glfwCloseWindow(); break;
            case GLFW_KEY_SPACE: break;
        }
        update_matrices();
    }
    
    
    void mouse_button(int button, int action) {
        if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS) {
            old_rotation_ = rotation_;
            int x_i, y_i;
            glfwGetMousePos(&x_i, &y_i);
            const float x = 2.0f * static_cast<float>(x_i) / width_ - 1.0f;
            const float y = 1.0f - 2.0f * static_cast<float>(y_i) / height_;
            trackball_.BeginDrag(x, y);
        }
    }
    
    void mouse_move(int x, int y, int old_x, int old_y){
        bool view_matrices_changed = false;
        
        // Maya style controls.
        if (glfwGetMouseButton(GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS) {
            const float x_f = 2.0f * static_cast<float>(x) / width_ - 1.0f;
            const float y_f = 1.0f - 2.0f * static_cast<float>(y) / height_;
            trackball_.Drag(x_f, y_f);
            rotation_ = trackball_.incremental_rotation() * old_rotation_;
            view_matrices_changed = true;
        }
        
        const float dx = x - old_x;
        const float dy = y - old_y;
        
        // Pan
        if (glfwGetMouseButton(GLFW_MOUSE_BUTTON_MIDDLE) == GLFW_PRESS) {
            const float scale = 0.05f;
            translation_ *= Eigen::Affine3f(Eigen::Translation3f(scale * dx, -scale * dy, 0.0f)).matrix();
            view_matrices_changed = true;
        }
        
        // Zoom
        if (glfwGetMouseButton(GLFW_MOUSE_BUTTON_RIGHT) == GLFW_PRESS) {
            const float scale = 0.05f;
            translation_ *= Eigen::Affine3f(Eigen::Translation3f(0.0f, 0.0f, scale * dy)).matrix();
            view_matrices_changed = true;
        }
        
        if (view_matrices_changed) {
            update_matrices();
        }
    }
    
    void mouse_pos(int x, int y) {
        static int old_x = x;
        static int old_y = y;
        mouse_move(x, y, old_x, old_y);
        old_x = x;
        old_y = y;
    }
    
    void hook_trackball_callbacks( void (*update_matrices)(const Eigen::Matrix4f&, vec3, vec3, vec3 )  , vec3 look, vec3 pos, vec3 up){
        /// Save the callback
        _update_matrices = update_matrices;
        c_look = look;
        c_pos = pos;
        c_up = up;
        assert(_update_matrices!=NULL);
                
        translation_ = Eigen::Matrix4f::Identity();
        rotation_ = Eigen::Matrix4f::Identity();
        old_rotation_ = Eigen::Matrix4f::Identity();
        scale_ = Eigen::Matrix4f::Identity();
        
        glfwSetKeyCallback(keyboard);
        glfwSetMouseButtonCallback(mouse_button);
        glfwSetMousePosCallback(mouse_pos);
        glfwSetWindowSizeCallback(resize);
    }

	float pick_move_factor(bool* movement_bool) {
			float move_factor = 0.0f;
			switch(move_state) {
			case MOVE_ACCELERATE:
				move_factor = movement[move_next];
				if (move_next == MOVEMENT_ARRAY_SIZE-1) {
					move_state = MOVE_CRUISE;
				} else if (move_next < MOVEMENT_ARRAY_SIZE && move_next >= 0) {
					move_next++;
				} else {
					cerr << "move_next: 'array out of bounds' is comming...!" << endl;
				}
				break;
			case MOVE_DECELERATE:
				move_factor = movement[move_next];
				if (move_next == 0) {
					*movement_bool = false;
					move_state = MOVE_STILL;
				} else if (move_next < MOVEMENT_ARRAY_SIZE && move_next >= 0) {
					move_next--;
				} else {
					cerr << "move_next: 'array out of bounds' is comming...!" << endl;
				}
				break;
			case MOVE_CRUISE:
				move_factor = movement[MOVEMENT_ARRAY_SIZE-1];
				break;
			case MOVE_STILL:
				cerr << "cannot be 'moving forward' and 'stand still' at the same time!" << endl;
				break;
			default:
				cerr << "move is in unknow/illegal state!" << endl;
			}
			return move_factor/3;
	}
    
    void keyboardCheck() {
        c_direction = (c_look-c_pos).normalized();
        angle_displacement =  tan(10.0)*c_direction.size();
        if (movingForward) {
			c_pos += pick_move_factor(&movingForward)*c_direction;
        }
        if (movingBackward) {
            c_pos -= pick_move_factor(&movingBackward)*c_direction;
        }
        if (lookingLeft) {
            cam_look_normal = cross(c_up,c_direction).normalized();
            cam_look_normal = cam_look_normal*angle_displacement;
            c_look += 0.1*cam_look_normal;
        }
        if (lookingRight) {
            cam_look_normal = cross(c_up,c_direction).normalized();
            cam_look_normal = cam_look_normal*angle_displacement;
            c_look -= 0.1*cam_look_normal;
        }
        if (lookingUp) {
            cam_look_normal = c_up.normalized()*angle_displacement;
            c_look += 0.1*cam_look_normal;
        }
        if (lookingDown) {
            cam_look_normal = c_up.normalized()*angle_displacement;
            c_look -= 0.1*cam_look_normal;
        }
        
        update_matrices();
    }
}

    void cameraMove(void (*update_matrices)(const Eigen::Matrix4f&, vec3, vec3, vec3 ), vec3 look, vec3 pos, vec3 up){
        hook_trackball_callbacks(update_matrices, look, pos, up);
    }

    void updateCamera() {
        keyboardCheck();
    }
    
} // opengp::
