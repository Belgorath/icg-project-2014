#version 330 core
layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec2 uvCoord;

out vec4 ShadowCoord, refletCoord;
out vec2 uv;

out vec3 light_dir, view_dir, position;

uniform vec3 light_pos;
uniform mat4 M;
uniform mat4 V;
uniform mat4 P;
uniform mat4 lightOffsetMVP;
uniform mat4 refletOffsetMVP;

void main(){
    vec4 position_mv = V * M * vec4(4*vertexPosition, 1);
    gl_Position = P * position_mv;
    
 	vec4 light_pos_mv = V*vec4(light_pos,1.0);
    light_dir = light_pos_mv.xyz - position_mv.xyz;
    view_dir = -position_mv.xyz;
    ShadowCoord = lightOffsetMVP * vec4(4*vertexPosition, 1);
    refletCoord = refletOffsetMVP * vec4(4*vertexPosition, 1);
    uv = uvCoord;
}