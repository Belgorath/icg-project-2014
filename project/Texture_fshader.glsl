#version 330 core
layout(location = 0) out vec3 color;


uniform sampler1D permutation_tex;
uniform sampler1D gradients_tex;
uniform sampler2D cheatFBM_tex;
float exponent_array[6];

float perm(float x) {
    return 255*texture(permutation_tex, x).r;
}

vec2 grad(float x) {
    return texture(gradients_tex, x).xy;
}

vec4 mod289(vec4 x)
{
  return x - floor(x * (1.0 / 289.0)) * 289.0;
}

vec4 permute(vec4 x)
{
  return mod289(((x*34.0)+1.0)*x);
}

vec4 taylorInvSqrt(vec4 r)
{
  return 1.79284291400159 - 0.85373472095314 * r;
}

vec2 fade(vec2 t) {
  return t*t*t*(t*(t*6.0-15.0)+10.0);
}

// Classic Perlin noise from https://github.com/ashima/webgl-noise
float cnoise(vec2 P)
{
  P = P*0.3;
  vec4 Pi = floor(P.xyxy) + vec4(0.0, 0.0, 1.0, 1.0);
  vec4 Pf = fract(P.xyxy) - vec4(0.0, 0.0, 1.0, 1.0);
  Pi = mod289(Pi); 
  vec4 ix = Pi.xzxz;
  vec4 iy = Pi.yyww;
  vec4 fx = Pf.xzxz;
  vec4 fy = Pf.yyww;

  vec4 i = permute(permute(ix) + iy);

  vec4 gx = fract(i * (1.0 / 41.0)) * 2.0 - 1.0 ;
  vec4 gy = abs(gx) - 0.5 ;
  vec4 tx = floor(gx + 0.5);
  gx = gx - tx;

  vec2 g00 = vec2(gx.x,gy.x);
  vec2 g10 = vec2(gx.y,gy.y);
  vec2 g01 = vec2(gx.z,gy.z);
  vec2 g11 = vec2(gx.w,gy.w);

  vec4 norm = taylorInvSqrt(vec4(dot(g00, g00), dot(g01, g01), dot(g10, g10), dot(g11, g11)));
  g00 *= norm.x;  
  g01 *= norm.y;  
  g10 *= norm.z;  
  g11 *= norm.w;  

  float n00 = dot(g00, vec2(fx.x, fy.x));
  float n10 = dot(g10, vec2(fx.y, fy.y));
  float n01 = dot(g01, vec2(fx.z, fy.z));
  float n11 = dot(g11, vec2(fx.w, fy.w));

  vec2 fade_xy = fade(Pf.xy);
  vec2 n_x = mix(vec2(n00, n01), vec2(n10, n11), fade_xy.x);
  float n_xy = mix(n_x.x, n_x.y, fade_xy.y);
  float ret = n_xy;
  return  0.5-abs((ret));

}

// From http://www8.cs.umu.se/kurser/TDBD12/HT01/papers/MusgraveTerrain00.pdf
float hybridMultifractal(vec2 point){
  float frequency;
  float result;
  float signal;
  float weight;
  float remainder;
  int i;
  float octaves = 6.0; //6 to 10
  int octavesINT = 6;

  float lacunarity = 2.0;
  float h = 0.1;
  float offset = 0.7;
  bool first = true;
  if (first) {
    frequency = 200;
    for (i=0; i<octavesINT; i++) {
      exponent_array[i] = pow(frequency, -h);
      frequency *= lacunarity;
      }
    first = false;
  }
  result = ( cnoise( point ) + offset ) * exponent_array[0];
  weight = result;
  point.x *= lacunarity;
  point.y *= lacunarity;
  for (i=1; i<octavesINT; i++) {
    if ( weight > 1.0 ){
      weight = 1.0;
    }
    signal = ( cnoise( point ) + offset ) * exponent_array[i];
    result += weight * signal;
    weight *= signal;
    point.x *= lacunarity;
    point.y *= lacunarity;
  }
  remainder = octaves - octavesINT;
  if(remainder != 0.0){
    result += (remainder * cnoise( point ) * exponent_array[i]);
  }
return result;
}

void main(){
   
        color = vec3(hybridMultifractal(vec2(0.01*gl_FragCoord.x,0.01*gl_FragCoord.y)));
    
}
