#version 330 core
in vec2 uv;
in vec4 ShadowCoord;

in vec3 light_dir, view_dir, position,normal_v;
in float height_offset;

layout(location = 0) out vec4 color;

uniform vec3 Ia, Id, Is;
uniform float p;
uniform sampler2D fbm_tex;
uniform sampler2D terrain_grass_tex;
uniform sampler2D terrain_rock_tex;
uniform sampler2D terrain_snow_tex;
uniform sampler2D terrain_sand_tex;
uniform sampler2DShadow light_tex;

// returns the porportion of each texture to introduce in the texture
// (sand, grass, rock, snow)
vec4 interpolation(vec3 normal) {
    float x = dot(normal, vec3(0.0, 0.0, 1.0));
    float r = clamp(x*x - 0.1, 0.0, 1.0);
    vec4 result = vec4(0.0, 0.0, 1-r, 0.0);
    
    float MIN_SAND = 0.02;
    float MAX_SAND = 0.04;
    float MIN_SNOW = 0.14;
    float MAX_SNOW = 0.20;
    
    if (height_offset < MAX_SAND) {
        float m = clamp((height_offset-MIN_SAND)/(MAX_SAND-MIN_SAND), 0.0, 1.0);
        result += r*vec4(1-m, m, 0.0, 0.0);
    } else if (height_offset < MIN_SNOW) {
        result += r*vec4(0.0, 1.0, 0.0, 0.0);
    } else {
        float n = clamp((height_offset-MIN_SNOW)/(MAX_SNOW-MIN_SNOW), 0.0, 1.0);
        result += r*vec4(0.0, 1-n, 0.0, 1.6*n);
    }
    return result;
     
}

void main(){
    
	////Shading
	vec3 normal = normalize(normal_v);
	vec3 light_dir_1 = normalize(light_dir);
	vec3 view_dir_1 = normalize(view_dir);
    
    
    ////Terrain coloring
    vec3 t_grass = texture(terrain_grass_tex, 10*uv).xyz;
    vec3 t_rock = texture(terrain_rock_tex, 30*uv).xyz;
    vec3 t_snow = texture(terrain_snow_tex, 30*uv).xyz;
    vec3 t_sand = texture(terrain_sand_tex, 60*uv).xyz;
    
    vec4 props = interpolation(normal);
    vec3 ka = props.r*t_sand + props.g*t_grass + props.b*t_rock + props.a*t_snow;
    
    // Water depth (blue effect)
    if (height_offset < 0) {
        float d = clamp(-15*height_offset, 0.5, 0.8);
        ka = d*vec3(0.3, 0.5, 0.6) + (1-d)*ka;
    }
    
    
    float bias = 0.001;
    vec3 kd = ka / vec3(2);
    
    vec3 ks = vec3(0.05);
    
    // 1) compute ambient term.
    vec3 ambient = Ia*ka;
    // 2) compute diffuse term.
    vec3 diffuse = Id*kd*max(0.0,dot(normal,light_dir_1));
    // 3) compute specular term.
    vec3 reflection_light = normalize(reflect(light_dir_1,normal));
    vec3 specular = Is*ks*pow(max(0.0,dot(view_dir_1,reflection_light)),p);
    
    float visibility = texture(light_tex, vec3((ShadowCoord.xy/ShadowCoord.w), (ShadowCoord.z)/ShadowCoord.w - bias));
    if(visibility < 0.65){
       visibility = 0.65;
    }
    if (height_offset < 0) {
        visibility = 1.0;
    }
    
    color = vec4((ambient+diffuse+specular) * visibility,1.0);
}
