// Copyright (C) 2014 - LGG EPFL

#include <iostream>
#include <sstream>
#include "common.h"
#include <stdlib.h>
#include <OpenGP/Surface_mesh.h>

#include "Terrain_vshader.h"
#include "Terrain_fshader.h"

#include "Texture_vshader.h"
#include "Texture_fshader.h"

#include "Sea_vshader.h"
#include "Sea_fshader.h"

#include "Sky_vshader.h"
#include "Sky_fshader.h"

#include "Sun_vshader.h"
#include "Sun_fshader.h"

#include "LightDepth_vshader.h"
#include "LightDepth_fshader.h"

#include "Curve_vshader.h"
#include "Curve_fshader.h"


#include "cube.h"
#include "camera_move.h"
#include "camera_fps.h"
#include "textures.h"

#define MODE_AUTO   1
#define MODE_FLY    2
#define MODE_FPS    3

using namespace std;
using namespace opengp;

// Camera mode control
static int cameraMode = MODE_FLY;


///Model-view-projection
mat4 model;
mat4 view;
mat4 projection;
mat4 MVP;
mat4 lightVP;
mat4 offsetMatrix;
mat4 negModel;

vec3 cam_pos;
vec3 cam_look;
vec3 cam_up;
vec3 light_pos;
vec3 light_look;
vec3 light_up;


/// Mesh
unsigned int n_indices;
GLuint vertexbuffer;
GLuint indexbuffer;
GLuint uvbuffer;
unsigned int mesh_n_indices;
GLuint mesh_vertexbuffer;
GLuint colorbuffer;
GLuint mesh_uvbuffer;

// Program ID
GLuint terrainProgramID;
GLuint textureProgramID;
GLuint seaProgramID;
GLuint skyProgramID;
GLuint lightDepthProgramID;
GLuint sunProgramID;
GLuint curveProgramID;


// Buffers
GLuint framebuffer, framebuffer2;

float t;
std::vector<vec3> curve_vertices;

/** Generates a grid made of n quads reaching from [-1, -1, 0] to [1, 1, 0] */
void bindPlane(unsigned int n) {
    const unsigned int nVertSide = n+1;
    const unsigned int nvertTotal = nVertSide*nVertSide;
    const float delta = 2.0/(float)n;
    const vec3 vOrigin = vec3(0.0, 0.0, 0.0);
    std::vector<vec3> vertices;
    std::vector<unsigned int> indices;
    std::vector<vec2> uvs;
    std::vector<vec3> colors;
    
    for (unsigned int i = 0; i < nVertSide; ++i) {
        for (unsigned int j = 0; j < nVertSide; ++j) {
            float x = i*2.0/(float)n - 1;
            float z = j*2.0/(float)n - 1;
            vertices.push_back(vOrigin + vec3(x, z, 0));
            uvs.push_back(vec2( ((float)x+1.0)/2.0 , ((float)z+1.0)/2.0 ));
        }
    }
    
    for (unsigned int i = 0; i < n; ++i) {
        for (unsigned int j = 0; j < n; ++j) {
            indices.push_back(i*nVertSide + j);
            indices.push_back(i*nVertSide + j + 1);
            indices.push_back((i+1)*nVertSide + j + 1);
            indices.push_back(i*nVertSide + j);
            indices.push_back((i+1)*nVertSide + j + 1);
            indices.push_back((i+1)*nVertSide + j);
        }
    }
    
    for (unsigned int i = 0; i < nvertTotal; ++i) {
        int nAdjTriangles = 0;
        vec3 normal(0.0, 0.0, 0.0);
    }
    
    n_indices = indices.size();
    
    glGenBuffers(ONE, &indexbuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexbuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);

    glGenBuffers(ONE, &vertexbuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(vec3), &vertices[0], GL_STATIC_DRAW);

    glGenBuffers(ONE, &uvbuffer);
    glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
    glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(vec2), &uvs[0], GL_STATIC_DRAW);
}


/** Generates a cube made of 6 quads reaching from [-50, -50, -50] to [50, 50, 50] */
void bindBox() {
    n_indices = nCubeVertices;
    glGenBuffers(ONE, &vertexbuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    glBufferData(GL_ARRAY_BUFFER, nCubeVertices * sizeof(vec3), &cubeVertices[0], GL_STATIC_DRAW);
    glGenBuffers(ONE, &uvbuffer);
    glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
    glBufferData(GL_ARRAY_BUFFER, nCubeUVs * sizeof(vec2), &cubeUVs[0], GL_STATIC_DRAW);
}

vec3 calculateBezierPoint(float t, vec3 p0, vec3 p1, vec3 p2, vec3 p3) {
    float u = 1-t;
    float tt = t*t;
    float u2 = pow(u, 2.0f);
    float u3 = pow(u, 3.0f);
    float t2 = pow(t, 2.0f);
    float t3 = pow(t, 3.0f);
    
    return (u3)*p0 + (3*u2*t)*p1 + (3*u*t2)*p2 + (t3)*p3;
}

void bindCurve() {
    int n_curves = 2;
    int n_points = 3*n_curves + 1;
    std::vector<vec3> points;
    points.push_back(vec3(  0.0,- 5.0,  2.0));
    
    points.push_back(vec3(-10.0,- 5.0,  2.5));
    points.push_back(vec3(-10.0, 10.0,  3.0));
    
    points.push_back(vec3(  0.0, 10.0,  3.5));
    
    points.push_back(vec3( 10.0, 10.0,  4.0));
    points.push_back(vec3( 10.0,-10.0,  4.5));
    
    points.push_back(vec3(  0.0,-10.0,  5.0));

    for (int i=0; i < n_curves; i++) {
        for (int j = 0; j < 100; j++) {
            float t = j/100.0f;
            vec3 p = calculateBezierPoint(t, points[3*i], points[3*i+1], points[3*i+2], points[3*i+3]);
            curve_vertices.push_back(p);
        }
    }
    
    n_indices = n_curves * 100;
    
    glGenBuffers(ONE, &vertexbuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    glBufferData(GL_ARRAY_BUFFER, n_indices * sizeof(vec3), &curve_vertices[0], GL_STATIC_DRAW);
}

/** Model, projection, view and camera */
void update_matrix_stack(const mat4& _model, vec3 look, vec3 pos, vec3 up){
    model = _model;
    projection = Eigen::perspective(45.0f, 4.0f/3.0f, 0.1f, 200.0f);
    cam_pos = pos;
    cam_look = look;
    cam_up = up;
    view = Eigen::lookAt(cam_pos, cam_look, cam_up);
    MVP = projection * view * model;
}

/** Light and material properties */
void define_light_material_properties(GLuint program, float t) {
   
    light_pos = vec3(6.5*cos(t),6.5*sin(t),3.0);
    light_look = vec3(0.0, 0.0, 0.0);
    light_up = vec3(0.0, 0.0, 1.0);
    vec3 Ia(1.0f, 1.0f, 1.0f);
    vec3 Id(1.0f, 1.0f, 1.0f);
    vec3 Is(1.0f, 1.0f, 1.0f);
    GLuint light_pos_id = glGetUniformLocation(program, "light_pos");
    GLuint Ia_id = glGetUniformLocation(program, "Ia");
    GLuint Id_id = glGetUniformLocation(program, "Id");
    GLuint Is_id = glGetUniformLocation(program, "Is");
    glUniform3fv(light_pos_id, ONE, light_pos.data());
    glUniform3fv(Ia_id, ONE, Ia.data());
    glUniform3fv(Id_id, ONE, Id.data());
    glUniform3fv(Is_id, ONE, Is.data());
    
    float p = 10.0f;
    GLuint p_id = glGetUniformLocation(program, "p");
    glUniform1f(p_id, p);
}

void init_shaders() {
    terrainProgramID    = compile_shaders( Terrain_vshader, Terrain_fshader );
    textureProgramID    = compile_shaders( Texture_vshader, Texture_fshader );
    seaProgramID        = compile_shaders( Sea_vshader, Sea_fshader );
    skyProgramID        = compile_shaders( Sky_vshader, Sky_fshader );
    lightDepthProgramID = compile_shaders( LightDepth_vshader, LightDepth_fshader );
    sunProgramID        = compile_shaders( Sun_vshader, Sun_fshader );
    curveProgramID      = compile_shaders( Curve_vshader, Curve_fshader );
    if(!terrainProgramID || !textureProgramID || !seaProgramID ||
       !skyProgramID || !lightDepthProgramID || !sunProgramID ||
       !curveProgramID) {
        exit(EXIT_FAILURE);
    }
}


void drawIndexedTriangles() {
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    glVertexAttribPointer(0,3,GL_FLOAT,DONT_NORMALIZE, ZERO_STRIDE, ZERO_BUFFER_OFFSET);
    glEnableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
    glVertexAttribPointer(1,2,GL_FLOAT,DONT_NORMALIZE, ZERO_STRIDE, ZERO_BUFFER_OFFSET);
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexbuffer);
    glDrawElements(GL_TRIANGLES, n_indices, GL_UNSIGNED_INT, 0);
    
    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
}

/** Vertex Array, Shader Programs, Noise texture in FBO */
void init() {
    // Creates the Vertex Array
    GLuint VertexArrayID;
    glGenVertexArrays(ONE, &VertexArrayID);
    glBindVertexArray(VertexArrayID);

    init_shaders();
    init_textures();
    
    // FBO
    glGenFramebuffers(1, &framebuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, fbm_tex, 0);
    
    glViewport(0, 0, 512, 512);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glUseProgram(textureProgramID);
    
    bindPlane(12);
    
    // Uniforms

    glUniform1i(glGetUniformLocation(textureProgramID, "permutation_tex"), 0);
    glUniform1i(glGetUniformLocation(textureProgramID, "gradients_tex"), 1);
    glUniformMatrix4fv(glGetUniformLocation(textureProgramID, "MVP"), 1, GL_FALSE, MVP.data());
    
    drawIndexedTriangles();
    
    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, fbm_tex);
    glGenerateMipmap(GL_TEXTURE_2D);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    
    // End of init

    vec3 l = vec3(-7.0f, -7.0f, -7.0f);
    vec3 p = vec3(7.0f, 7.0f, 7.0f);
    vec3 u = vec3(0.0f, 0.0f, 1.0f);
    update_matrix_stack(mat4::Identity(),l,p,u);
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_MULTISAMPLE);
    offsetMatrix <<
    0.5, 0.0, 0.0, 0.5,
    0.0, 0.5, 0.0, 0.5,
    0.0, 0.0, 0.5, 0.5,
    0.0, 0.0, 0.0, 1.0;

    glGenFramebuffers(1, &framebuffer2);
}


void displayLightDepth(float t, bool progressiveHeight){
    // Context
    glBindFramebuffer(GL_FRAMEBUFFER, framebuffer2);
    glViewport(0,0,1024,1024);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, light_tex, 0);
    glDrawBuffer(GL_NONE);
    glUseProgram(lightDepthProgramID);
    
    // Binding
    bindPlane(128);
    
    // Uniforms
    float fieldOfView = 45.0f;
    float aspectRatio = 4.0f/3.0f;
    float nearPlane = 1.0f;
    float farPlane  = 100.f;
    lightVP = Eigen::perspective(fieldOfView,aspectRatio,nearPlane,farPlane)*Eigen::lookAt(light_pos,light_look,light_up);
    glUniform1i(glGetUniformLocation(lightDepthProgramID, "fbm_tex"), 2);
    glUniformMatrix4fv(glGetUniformLocation(lightDepthProgramID, "lightVP"), ONE, GL_FALSE,lightVP.data());
    glUniform1f(glGetUniformLocation(lightDepthProgramID,"t"),t);
    glUniform1i(glGetUniformLocation(lightDepthProgramID,"progressiveHeight"),progressiveHeight);

    // Drawing
    drawIndexedTriangles();

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}


void displayTerrain(float t, bool progressiveHeight) {
    // Context
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glViewport(0, 0, 1024, 768);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glUseProgram(terrainProgramID);
    
    // Binding
    bindPlane(128);
    
    // Uniforms
    
    mat4 lightOffsetMVP = offsetMatrix*lightVP;
    define_light_material_properties(terrainProgramID, t);
    glUniform1i(glGetUniformLocation(terrainProgramID, "fbm_tex"), 2);
    glUniform1i(glGetUniformLocation(terrainProgramID, "terrain_grass_tex"), 3);
    glUniform1i(glGetUniformLocation(terrainProgramID, "terrain_snow_tex"), 4);
    glUniform1i(glGetUniformLocation(terrainProgramID, "terrain_rock_tex"), 5);
    glUniform1i(glGetUniformLocation(terrainProgramID, "terrain_sand_tex"), 6);
    glUniform1i(glGetUniformLocation(terrainProgramID, "light_tex"), 8);
    glUniformMatrix4fv(glGetUniformLocation(terrainProgramID, "M"), 1, GL_FALSE, model.data());
    glUniformMatrix4fv(glGetUniformLocation(terrainProgramID, "V"), 1, GL_FALSE, view.data());
    glUniformMatrix4fv(glGetUniformLocation(terrainProgramID, "P"), 1, GL_FALSE, projection.data());
    glUniformMatrix4fv(glGetUniformLocation(terrainProgramID, "lightOffsetMVP"), 1, GL_FALSE, lightOffsetMVP.data());
    glUniform1f(glGetUniformLocation(terrainProgramID,"t"),t);
    glUniform1i(glGetUniformLocation(terrainProgramID,"progressiveHeight"),progressiveHeight);
    
    // Drawing
    drawIndexedTriangles();
}

void displaySea(float t) {
    // Context
    glUseProgram(seaProgramID);
    
    // Binding
    bindPlane(1);
    
    // Uniforms
    mat4 lightOffsetMVP = offsetMatrix*lightVP;
    mat4 refletOffsetMVP = offsetMatrix *projection*view*negModel;
    define_light_material_properties(seaProgramID, t);
    glUniformMatrix4fv(glGetUniformLocation(seaProgramID, "M"), 1, GL_FALSE, model.data());
    glUniformMatrix4fv(glGetUniformLocation(seaProgramID, "V"), 1, GL_FALSE, view.data());
    glUniformMatrix4fv(glGetUniformLocation(seaProgramID, "P"), 1, GL_FALSE, projection.data());
    glUniform1i(glGetUniformLocation(seaProgramID, "light_tex"), 8);
    glUniform1i(glGetUniformLocation(seaProgramID, "water_tex"), 9);
    glUniform1i(glGetUniformLocation(seaProgramID, "relflex_tex"), 10);
    glUniformMatrix4fv(glGetUniformLocation(seaProgramID, "lightOffsetMVP"), 1, GL_FALSE, lightOffsetMVP.data());
    glUniformMatrix4fv(glGetUniformLocation(seaProgramID, "refletOffsetMVP"), 1, GL_FALSE, refletOffsetMVP.data());
    glUniform1f(glGetUniformLocation(seaProgramID,"t"),t);
    
    // Drawing
    glEnable (GL_BLEND);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    drawIndexedTriangles();
}

void displaySky(float t) {
    // Context
    glUseProgram(skyProgramID);
    
    // Binding
    bindBox();
    
    // Uniforms
    glUniform1i(glGetUniformLocation(skyProgramID, "skybox_tex"), 7);
    glUniformMatrix4fv(glGetUniformLocation(skyProgramID, "MVP"), 1, GL_FALSE, MVP.data());
    
    // Drawing
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    glVertexAttribPointer(0,3,GL_FLOAT,DONT_NORMALIZE, ZERO_STRIDE, ZERO_BUFFER_OFFSET);
    glEnableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
    glVertexAttribPointer(1,2,GL_FLOAT,DONT_NORMALIZE, ZERO_STRIDE, ZERO_BUFFER_OFFSET);

    glDrawArrays(GL_TRIANGLES,0,n_indices);

    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
}

void displaySun(float t) {
    glUseProgram(sunProgramID);
    
    std::vector<vec3> vertices;
    vertices.push_back(light_pos);
    
    glGenBuffers(ONE, &vertexbuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(vec3), &vertices[0], GL_STATIC_DRAW);
    
    define_light_material_properties(sunProgramID, t);
    glUniformMatrix4fv(glGetUniformLocation(sunProgramID, "P"), ONE, GL_FALSE,projection.data());
    glUniformMatrix4fv(glGetUniformLocation(sunProgramID, "V"), ONE, GL_FALSE,view.data());
    glUniformMatrix4fv(glGetUniformLocation(sunProgramID, "M"), ONE, GL_FALSE,model.data());
    
    glEnable (GL_BLEND);
    glEnable(GL_PROGRAM_POINT_SIZE);
    glPointParameterf(GL_POINT_FADE_THRESHOLD_SIZE, 1.0);

    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDrawArrays(GL_POINTS, 0, 1);
    glDisableVertexAttribArray(0);
    
    
}

void displayBezierCurve(float t, bool cameraAutomaticPath, bool displayPath) {
    // Context
    glUseProgram(curveProgramID);
    
    // Binding
    bindCurve();
    
    // Uniforms
    glUniformMatrix4fv(glGetUniformLocation(curveProgramID, "MVP"), 1, GL_FALSE, MVP.data());
    
    // Drawing
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    glVertexAttribPointer(0,3,GL_FLOAT,DONT_NORMALIZE, ZERO_STRIDE, ZERO_BUFFER_OFFSET);
    
    if (displayPath) {
        glDrawArrays(GL_LINE_STRIP,0,n_indices);
    }
    
    if (cameraAutomaticPath) {
        update_matrix_stack(mat4::Identity(), vec3(0.0, 0.0, 0.0), curve_vertices[(int)100*t], vec3(0.0, 0.0, 1.0));
    }
}


void display() {

    static float t = 0.;
    static bool cameraAutomaticPath = false;
    if (cameraMode == MODE_AUTO) {
        cameraAutomaticPath = true;
    }
    static bool displayPath = false;
    static bool progressiveHeight = true;
    displayLightDepth(t, progressiveHeight);
    displayTerrain(t, progressiveHeight);
    displaySky(t);
    displaySea(t);
    displaySun(t);
    displayBezierCurve(t, cameraAutomaticPath, displayPath);
    if (!cameraAutomaticPath) {
        if(cameraMode == MODE_FPS) {
            updateCameraFPS();
        } else {
            updateCamera();
        }
    }
    t += 0.01;
}

int main(int, char**) {
    glfwInitWindowSize(1024, 768);
    glfwCreateWindow("ICG Project");
    glfwDisplayFunc(display);
    init();
	if (cameraMode == MODE_FPS) {
		cameraMoveFPS(update_matrix_stack, cam_look, cam_pos, cam_up);
	} else {
		cameraMove(update_matrix_stack, cam_look, cam_pos, cam_up);
	}
    glfwMainLoop();
    return EXIT_SUCCESS;
}
