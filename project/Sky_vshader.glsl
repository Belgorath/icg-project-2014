#version 330 core
layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec2 uvCoords;

uniform mat4 MVP;

out vec2 uv;

void main(){
    gl_Position = MVP * vec4(vertexPosition, 1);
    uv = uvCoords;
}