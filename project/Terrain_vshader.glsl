#version 330 core
layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec2 uvCoords;

out vec2 uv;
out vec4 ShadowCoord;

out vec3 light_dir, view_dir, position,normal_v;
out float height_offset;

uniform vec3 light_pos;
uniform mat4 M;
uniform mat4 V;
uniform mat4 P;
uniform mat4 lightOffsetMVP;
uniform sampler2D fbm_tex;
uniform float t;
uniform bool progressiveHeight;

vec4 light_pos_mv;

void main(){
    
    height_offset = texture(fbm_tex, uvCoords).x - 1.3;
    position = 4 * vec3(vertexPosition.xy, height_offset);
    
    if (progressiveHeight) {
        position.z = 4 * clamp(2*sqrt(abs(height_offset))*t, 0.01f, 1.0f) * height_offset;
    }
    
    vec4 position_mv = V * M * vec4(position, 1);
    gl_Position = P * position_mv;
    
    /* Normal computing */
    float bu[2] = float[](
        texture(fbm_tex, uvCoords-vec2(-1.0/128.0,0.0)).x/0.06f,
        texture(fbm_tex, uvCoords-vec2(1.0/128.0,0.0)).x/0.06f);
    float bv[2] = float[](
        texture(fbm_tex, uvCoords-vec2(0.0,-1.0/128.0)).x/0.06f,
        texture(fbm_tex, uvCoords-vec2(0.0,1.0/128.0)).x/0.06f);
    normal_v = vec3(bu[0]-bu[1],bv[0]-bv[1],0.8);
    light_pos_mv = V*vec4(light_pos,1.0);
    light_dir = light_pos_mv.xyz - position_mv.xyz;
    view_dir = -position_mv.xyz;
    ShadowCoord = lightOffsetMVP * vec4(position, 1);
    uv = uvCoords;
}