#version 330 core
layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec2 uvCoords;
uniform mat4 lightVP;
uniform sampler2D fbm_tex;
uniform float t;
uniform bool progressiveHeight;

void main(){
    
    float height_offset = texture(fbm_tex, uvCoords).x - 1.3;
    vec3 position = 4 * vec3(vertexPosition.xy, height_offset);
    
    if (progressiveHeight) {
        position.z = 4 * clamp(2*sqrt(abs(height_offset))*t, 0.01f, 1.0f) * height_offset;
    }
    
    vec4 position_mvp = lightVP * vec4(position, 1);
    gl_Position = position_mvp;

}
