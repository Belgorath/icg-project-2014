#version 330 core
in vec4 ShadowCoord, refletCoord;
in vec2 uv;

in vec3 light_dir, view_dir, position;

layout(location = 0) out vec4 color;

uniform vec3 Ia, Id, Is;
uniform float p;
uniform sampler2D water_tex;
uniform sampler2D relflex_tex;
uniform sampler2DShadow light_tex;
uniform float t;

void main(){
	////Terrain coloring
    vec3 ka = vec3(0.0);
    vec3 kd = vec3(0.0);
	vec3 ks = vec3(0.8);
    
	////Shading
    vec3 normal = texture(water_tex, 5*uv+0.5*t).xyz+texture(water_tex, 10*uv+0.7*t).xyz+0.8*texture(water_tex, 30*uv+0.9*t).xyz;
	normal = normalize(normal);
	vec3 light_dir_1 = normalize(light_dir);
	vec3 view_dir_1 = normalize(view_dir);
    // 1) compute ambient term.
    vec3 ambient = Ia*ka;
    // 2) compute diffuse term.
    vec3 diffuse = Id*kd*max(0.0,dot(normal,light_dir_1));
    // 3) compute specular term.
    vec3 reflection_light = normalize(reflect(light_dir_1,normal));
    vec3 specular = Is*ks*pow(max(0.0,dot(view_dir_1,reflection_light)),0.5);
    
    float bias = 0.001;

     float visibility = texture(light_tex, vec3((ShadowCoord.xy/ShadowCoord.w), (ShadowCoord.z)/ShadowCoord.w - bias));
    if(visibility < 0.5){
        visibility = 0.5;
    }
    color = vec4((ambient+diffuse+specular) * visibility, 0.3 + 0.3 * (1-visibility));
}