#version 330 core
layout(location = 0) in vec3 vertexPosition;

uniform mat4 MVP;

void main(){
    vec4 position_mvp = MVP * vec4(vertexPosition, 1);
    gl_Position = position_mvp;
}