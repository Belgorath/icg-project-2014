/* code from http://r3dux.org/2011/05/simple-opengl-keyboard-and-mouse-fps-controls/ */

#pragma once
#include <Eigen/Dense>
#include <common.h>
#include <OpenGP/GL/Trackball.h>

#define DEBUG_PRINT(A, ...) fprintf(stderr, "[DEBUG] (%d:%s): " A "\n", __LINE__,\
                                    __FILE__, ##__VA_ARGS__)

namespace {
static void (*_update_matrices_FPS)(const Eigen::Matrix4f&, vec3, vec3, vec3) = NULL;

const float TO_RADS = 3.141592654f / 180.0f; // The value of 1 degree in radians
const float TO_DEGS = 180.0f / 3.141592654f; // The value of 1 radian in degrees
 
const GLint windowWidth  = 1024;                    // Width of our window
const GLint windowHeight = 768;                    // Heightof our window
 
const GLint midWindowX = windowWidth  / 2;         // Middle of the window horizontally
const GLint midWindowY = windowHeight / 2;         // Middle of the window vertically

const GLfloat vertMouseSensitivity  = 20.0f;
const GLfloat horizMouseSensitivity = 20.0f;
 
// Camera rotation
GLfloat camXRot = 0.0f;
GLfloat camYRot = 0.0f;
// old rotation for x axis
GLfloat camXRotOld = 0.0f;
 
// Camera movement speed
vec3 camSpeed = vec3(0.0f,0.0f,0.0f);

vec3 fps_cam_pos;
vec3 fps_cam_up;
vec3 fps_cam_look;

// How fast we move (higher values mean we move faster)
GLfloat movementSpeedFactor = 0.1f;
 
// Hoding any keys down?
bool holdingForward     = false;
bool holdingBackward    = false;
bool holdingLeftStrafe  = false;
bool holdingRightStrafe = false;

// Function to convert degrees to radians
float toRads(const float &theAngleInDegrees)
{
    return theAngleInDegrees * TO_RADS;
}
// Function to convert degrees to radians
float toDegs(const float &theAngleInDegrees)
{
    return theAngleInDegrees * TO_DEGS;
}

void update_matrices(){
    assert(_update_matrices_FPS!=NULL);
    Eigen::Matrix4f model = translation_ * rotation_ * scale_;
	_update_matrices_FPS(mat4::Identity(), fps_cam_look, fps_cam_pos, fps_cam_up);
}

// Function to calculate which direction we need to move the camera and by what amount
void calculateCameraMovement() {
	vec3 fps_cam_dir = (fps_cam_look-fps_cam_pos).normalized();
	vec3 fps_cam_right = cross(fps_cam_dir,fps_cam_up).normalized();

	if (holdingForward) {
		camSpeed += movementSpeedFactor*fps_cam_dir;
	}
	if (holdingBackward) {
		camSpeed -= movementSpeedFactor*fps_cam_dir;
	}
	if (holdingLeftStrafe) {
		camSpeed -= movementSpeedFactor*fps_cam_right;
	}
	if (holdingRightStrafe) {
		camSpeed += movementSpeedFactor*fps_cam_right;
	}
}

// Function to set flags according to which keys are pressed or released
void handleKeypress(int theKey, int theAction) {
    switch(theKey) {
        case 'W':
            holdingForward = !holdingForward;
            break;
 
        case 'S':
            holdingBackward = !holdingBackward;
            break;
 
        case 'A':
            holdingLeftStrafe = !holdingLeftStrafe;
            break;
 
        case 'D':
            holdingRightStrafe = !holdingRightStrafe;
            break;

        default:
            // Do nothing...
            break;
    }
}

// Function to deal with mouse position changes, called whenever the mouse cursorm moves
void handleMouseMove(int mouseX, int mouseY)
{
    int horizMovement = mouseX - midWindowX;
    int vertMovement  = mouseY - midWindowY;
 
    camXRot += vertMovement / vertMouseSensitivity;
    camYRot += horizMovement / horizMouseSensitivity;
 
    // Control looking up and down with the mouse forward/back movement
    // Limit loking up to vertically up and down
    camXRot = min(max(camXRot, -89.0f), 89.0f);

	// DEBUG_PRINT("(camXRot,camYRot) = (%f, %f)", camXRot, camYRot);
 
    // Reset the mouse position to the centre of the window each frame
    glfwSetMousePos(midWindowX, midWindowY);
}

// Function to move the camera the amount we've calculated in the calculateCameraMovement function
void moveCamera()
{
	// update cam_pos and cam_look according to camXSpeed and camXRot
	vec3 fps_cam_dir = (fps_cam_look-fps_cam_pos).normalized();
	vec3 fps_cam_right = cross(fps_cam_dir,fps_cam_up).normalized();
	float delta_X = camXRot - camXRotOld;
	float delta_Y = camYRot;

	fps_cam_pos += camSpeed;
	
	fps_cam_dir -= delta_X*fps_cam_up/vertMouseSensitivity;
	fps_cam_dir += delta_Y*fps_cam_right/horizMouseSensitivity;
	fps_cam_dir.normalize();
	fps_cam_look = fps_cam_dir + fps_cam_pos;
	
	update_matrices();
	
	// update differential values
	camXRotOld = camXRot;
	camYRot = 0;
	camSpeed = vec3(0.0f,0.0f,0.0f);
}

void hook_callbacks( void (*update_matrices)(const Eigen::Matrix4f&, vec3, vec3, vec3 )  , vec3 look, vec3 pos, vec3 up){
        /// Save the callback
        _update_matrices_FPS = update_matrices;
        fps_cam_look = look;
        fps_cam_pos = pos;
        fps_cam_up = up;
		//just to set the original rotation according to initial pos and look:
		camXRot = toDegs(1.570796326f - acos(dot(fps_cam_up,(fps_cam_look-fps_cam_pos).normalized())));
		camXRotOld = camXRot;
        assert(_update_matrices_FPS!=NULL);
        
		glfwSetKeyCallback(handleKeypress);
		glfwSetMousePosCallback(handleMouseMove);
		glfwDisable(GLFW_MOUSE_CURSOR); // WARNING: known to cause troubles w/ some linux machines...
    }

void cameraMoveFPS(void (*update_matrices)(const Eigen::Matrix4f&, vec3, vec3, vec3 ), vec3 look, vec3 pos, vec3 up) {
	glfwSetMousePos(midWindowX, midWindowY);
	hook_callbacks(update_matrices, look, pos, up);
}

void updateCameraFPS() {
	// glCheckError(); Y U NO WORK?
	calculateCameraMovement();
	moveCamera();
}

}