// Copyright (C) 2014 - LGG EPFL
// Copyright (C) 2014 - Andrea Tagliasacchi

#pragma once
///--- LOAD THE OPENGL LIBRARIES HERE (IN CROSS PLATFORM)
#include <GL/glew.h> ///< must be before glfw
#include <GL/glfw.h>

/// We use a modified (to support OpenGL3) version of the Eigen OpenGL module 
/// @see http://eigen.tuxfamily.org/dox/unsupported/group__OpenGLSUpport__Module.html
#include <OpenGP/GL/EigenOpenGLSupport3.h>
// #include <Eigen/OpenGL3Support>

/// We use Eigen for linear algebra
typedef Eigen::Vector2f vec2;
typedef Eigen::Vector3f vec3;
typedef Eigen::Vector4f vec4;
typedef Eigen::Matrix4f mat4;
typedef Eigen::Matrix3f mat3;

/// On some OSs the exit flags are not defined
#ifndef EXIT_SUCCESS
    #define EXIT_SUCCESS 0
#endif
#ifndef EXIT_FAILURE
    #define EXIT_FAILURE 1
#endif

/// Utilities to parse shader and compile shader files
#include <OpenGP/GL/glfw_helpers.h>
#include <OpenGP/GL/glfw_trackball.h>

/// For mesh I/O we use OpenGP
#include <OpenGP/Surface_mesh.h>

/// These namespaces assumed by default
using namespace std;
using namespace opengp;

static inline const char* ErrorString(GLenum error) {
	const char* msg;

	switch (error) {
		#define Case(Token) case Token: msg = #Token; break;
		Case(GL_INVALID_ENUM);
		Case(GL_INVALID_VALUE);
		Case(GL_INVALID_OPERATION);
		Case(GL_INVALID_FRAMEBUFFER_OPERATION);
		Case(GL_NO_ERROR);
		Case(GL_OUT_OF_MEMORY);
		#undef Case
	}

	return msg;
}

static inline void _glCheckError(const char* file, int line) {
	GLenum error;
	while ((error = glGetError()) != GL_NO_ERROR) {
		fprintf(stderr, "ERROR: file %s, line %i: %s.\n", file, line,
		ErrorString(error));
	}
}

#ifndef NDEBUG
	#define glCheckError() _glCheckError(__FILE__, __LINE__)
#else
	#define glCheckError() ((void)0)
#endif