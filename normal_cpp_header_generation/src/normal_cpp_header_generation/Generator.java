package normal_cpp_header_generation;

public class Generator {

	public static void main(String[] args) {
		final int MAX = 19;
		double accumulator = 0.0;
		System.out.println("#define MOVEMENT_ARRAY_SIZE " + (MAX+1));
		System.out.print("static const float movement[] = { ");
		for (int i = 0; i <= MAX; i++) {
			double x = ((double)2 * (double)i / (double)MAX) - 1;
			accumulator += gaussian(x, 0, 1)*2/MAX;
			System.out.print(accumulator+",");
		}
	}

	private static double gaussian(double x, double mu, double sigma) {
		return (double)1 / (sigma * Math.sqrt((double)2 * Math.PI))
				* Math.exp(-(double)1 / (double)2 * ((x - mu) / sigma) * ((x - mu) / sigma));
	}
}
